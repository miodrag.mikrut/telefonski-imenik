package com.miodrag.mikrut.imenik.adapters;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.db.model.Kontakt;

import java.util.List;

public class MojRecyclerViewAdapterListContact extends
        RecyclerView.Adapter<MojRecyclerViewAdapterListContact.MyViewHolder> {

    public List<Kontakt> listKontakt;
    public OnRecyclerItemClickListener listener;

    public interface OnRecyclerItemClickListener {
        void onRVItemClick(Kontakt kontakt);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvIme;
        TextView tvPrezime;
        ImageView slika;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvIme = itemView.findViewById(R.id.tv_recycler_ime);
            tvPrezime = itemView.findViewById(R.id.tv_recycler_prezime);
            slika = itemView.findViewById(R.id.rv_image_kontakta);

        }

        public void bind(final Kontakt kontakt, final OnRecyclerItemClickListener listener) {
            tvIme.setText(kontakt.getFirstName());
            tvPrezime.setText(kontakt.getLastName());
            slika.setImageBitmap(BitmapFactory.decodeFile(kontakt.getImage_path()));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(kontakt);
                }
            });
        }
    }

    public MojRecyclerViewAdapterListContact(List<Kontakt> listKontakt, OnRecyclerItemClickListener listener) {
        this.listKontakt = listKontakt;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listKontakt.get(i), listener);

        int currentPostion = i;
        final Kontakt glumac = listKontakt.get(currentPostion);

        //brisanje glumca iz liste na long klik
//        myViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DeleteDialog alertDialog = new DeleteDialog(myViewHolder.itemView.getContext());
//                alertDialog.show();//napraviti da se priliko klika na yes obrse a na cancel odustane
//
//                notificationAboutRemovingFromList(myViewHolder);//notifikacija da je uklonjen glumac sa liste
//
//                removeGlumacOnLongClick(glumac);//uklanja glumca sa liste na long klik
//
//                return true;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return listKontakt.size();
    }


    public void addGlumac(Kontakt kontakt) {
        listKontakt.add(kontakt);
        notifyDataSetChanged();
    }
}
