package com.miodrag.mikrut.imenik.adapters;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.db.model.Broj;
import com.miodrag.mikrut.imenik.tools.Tools;

import java.util.List;

public class RVadapterListaBrojeva extends RecyclerView.Adapter<RVadapterListaBrojeva.MyViewHolder> {

    private List<Broj> rv_lista;




    public RVadapterListaBrojeva(List<Broj> rv_lista) {
        this.rv_lista = rv_lista;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView category;
        TextView broj;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            category = itemView.findViewById(R.id.rv_broj_phone_category);
            broj     = itemView.findViewById(R.id.rv_broj_phone);
        }

        public void bind(final Broj objekat) {
            category.setText(objekat.getPhone_category());
            broj.setText(objekat.getPhone_number());
            broj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tools.dialPhoneNumber(broj.getText().toString(),itemView.getContext());
                }
            });
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_single_item_broj,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(rv_lista.get(i));

    }

    @Override
    public int getItemCount() {
        return rv_lista.size();
    }

    //dodavanje i refresh rv liste
    public void setNewData(List<Broj> listaGrupa){
        this.rv_lista.clear();
        this.rv_lista.addAll(listaGrupa);
        notifyDataSetChanged();
    }

}
