package com.miodrag.mikrut.imenik.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.db.DatabaseHelper;
import com.miodrag.mikrut.imenik.db.model.Kontakt;
import com.miodrag.mikrut.imenik.tools.Tools;

import java.sql.SQLException;

public class Edit extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    private String imagePath;

    private EditText firstName;
    private EditText lastName;
    private ImageButton imageButton;
    private Button btn_save_changes;
    private Kontakt kontakt;
    private int kontakt_id;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        inicijalizacija();
        preuzmiPodatkeIzIntenta();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
            }
        });
        btn_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
                if(Tools.proveraPrefsPodesavanja("toast",Edit.this)){
                    Toast.makeText(Edit.this, kontakt+ " added" , Toast.LENGTH_SHORT).show();
                }
                if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)){
                    Tools.createNotification(Edit.this,kontakt,"add");
                }
            }
        });
    }

    private void inicijalizacija(){
            firstName = findViewById(R.id.edit_ev_first_name);
            lastName = findViewById(R.id.edit_ev_last_name);
            btn_save_changes = findViewById(R.id.edit_btn_save);
            imageButton = findViewById(R.id.edit_image_button);
    }

    private void preuzmiPodatkeIzIntenta(){
        kontakt_id = getIntent().getIntExtra("objekat_id",-1);
        if(kontakt_id < 0){
            Toast.makeText(this, "Greska! "+kontakt_id+" ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            kontakt = getDatabaseHelper().getKontaktDao().queryForId(kontakt_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        firstName.setText(kontakt.getFirstName());
        lastName.setText(kontakt.getLastName());
        imageButton.setImageBitmap(BitmapFactory.decodeFile(kontakt.getImage_path()));
    }

    private void saveChanges(){
        kontakt.setFirstName(firstName.getText().toString());
        kontakt.setLastName(lastName.getText().toString());

        kontakt.setImage_path(imagePath);

        try {
            getDatabaseHelper().getKontaktDao().createOrUpdate(kontakt);
            Toast.makeText(this, kontakt + " updated", Toast.LENGTH_SHORT).show();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Edit.this, Detail.class);
        intent.putExtra("objekat_id",kontakt_id);
        startActivity(intent);
    }

    /**
     * Da bi dobili pristup Galeriji slika na uredjaju
     * moramo preko URI-ja pristupiti delu baze gde su smestene
     * slike uredjaja. Njima mozemo pristupiti koristeci sistemski
     * ContentProvider i koristeci URI images/* putanju
     *
     * Posto biramo sliku potrebno je da pozovemo aktivnost koja icekuje rezultat
     * Kada dobijemo rezultat nazad prikazemo sliku i dobijemo njenu tacnu putanju
     * */
    //proverava dozvolu za biranje iz galerije
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED){
        }
    }
    //ako je dozvoljeno bira se slika iz galerije
    private void selectPicture(){
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    imagePath = picturePath; //uzima image path i stavlja ga da bude dostupno
                    cursor.close();

                    // String picturePath contains the path of selected Image
                    imageButton.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//
//                    dialog.show();

                    Toast.makeText(this, picturePath,Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
