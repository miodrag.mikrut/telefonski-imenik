package com.miodrag.mikrut.imenik.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.adapters.RVadapterListaBrojeva;
import com.miodrag.mikrut.imenik.db.DatabaseHelper;
import com.miodrag.mikrut.imenik.db.model.Broj;
import com.miodrag.mikrut.imenik.db.model.Kontakt;
import com.miodrag.mikrut.imenik.tools.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Detail extends AppCompatActivity {


    DatabaseHelper databaseHelper;

    private Toolbar toolbar;
    private Spinner spinner;
    private TextView firstName;
    private TextView lastName;
    private ImageView image;
    private Button btn_add_number;
    private Kontakt kontakt;
    private String image_path;
    private int objekat_id;
    private List<Broj> phone_list;
    private ListView listView_phone;
    private RecyclerView recyclerView;
    private RVadapterListaBrojeva rVadapterListaBrojeva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setupToolbar();
        inicijalizacija();
        preuzmiPodatkeIzIntenta();
        setupRV();

        btn_add_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Detail.this,AddNumber.class);
                intent.putExtra("objekat_id",kontakt.getId());
                startActivity(intent);
            }
        });
    }


    private void inicijalizacija(){
        firstName = findViewById(R.id.detail_tv_firstName);
        lastName = findViewById(R.id.detail_tv_lastName);
        image = findViewById(R.id.detail_imageview);
        phone_list = new ArrayList<>();
        recyclerView = findViewById(R.id.detail_rv_container);
        btn_add_number = findViewById(R.id.detail_btn_add_number);
    }


    private void preuzmiPodatkeIzIntenta(){
        objekat_id = getIntent().getIntExtra("objekat_id",-1);
        if(objekat_id < 0){
            Toast.makeText(this, "Greska! "+ objekat_id +" ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            kontakt = getDatabaseHelper().getKontaktDao().queryForId(objekat_id);
            phone_list = getDatabaseHelper().getBrojDao().queryForEq("kontakt_id",objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        firstName.setText(kontakt.getFirstName());
        lastName.setText(kontakt.getLastName());
        image.setImageBitmap(BitmapFactory.decodeFile(kontakt.getImage_path()));

    }

    //setuje toolbar
    private void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;
            case R.id.action_create:
                Intent intent = new Intent(this, Add.class);
                startActivity(intent);
                setTitle("New phone");
                if(Tools.proveraPrefsPodesavanja("toast",Detail.this)){
                    Toast.makeText(Detail.this, kontakt+ " added" , Toast.LENGTH_SHORT).show();
                }
                if(Tools.proveraPrefsPodesavanja("notifications",Detail.this)){
                    Tools.createNotification(Detail.this,kontakt,"add");
                }
                break;
            case R.id.action_edit:
                Intent intent1 = new Intent(Detail.this, Edit.class);
                intent1.putExtra("objekat_id",kontakt.getId());
                startActivity(intent1);
                break;
            case R.id.action_delete:
                delete();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //^^^ kraj setupa za toolbar

    //brise glumca i sve filmove koji su vezani za id tog glumca
    private void delete() {
        AlertDialog dialogDelete = new AlertDialog.Builder(this)
                .setTitle("Delete group")
                .setMessage("Are you sure you want to permanently delete the group?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            List<Broj> slike = getDatabaseHelper().getBrojDao().queryForEq("kontakt_id", kontakt.getId());

                            getDatabaseHelper().getKontaktDao().delete(kontakt);
                            for (Broj slika : slike) {
                                getDatabaseHelper().getBrojDao().delete(slika);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                            Toast.makeText(Detail.this, kontakt + " deleted.", Toast.LENGTH_SHORT).show();
                        }
                        if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                            Tools.createNotification(Detail.this, kontakt, "delete");
                        }
                        Intent intent = new Intent(Detail.this, Main.class);
                        startActivity(intent);
                        if(Tools.proveraPrefsPodesavanja("toast",Detail.this)){
                            Toast.makeText(Detail.this, kontakt+ " added" , Toast.LENGTH_SHORT).show();
                        }
                        if(Tools.proveraPrefsPodesavanja("notifications",Detail.this)){
                            Tools.createNotification(Detail.this,kontakt,"delete");
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }


    //stavljanj RV adatera
    private void setupRV(){
        recyclerView = findViewById(R.id.detail_rv_container);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        rVadapterListaBrojeva = new RVadapterListaBrojeva(phone_list);

        recyclerView.setAdapter(rVadapterListaBrojeva);


    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
