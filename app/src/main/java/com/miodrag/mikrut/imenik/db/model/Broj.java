package com.miodrag.mikrut.imenik.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "brojevi")
public class Broj {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "phone_number")
    private String phone_number;
    @DatabaseField(columnName = "phone_category")
    private String phone_category;
    @DatabaseField(foreign = true,foreignAutoRefresh = true,foreignAutoCreate = true)
    private Kontakt kontakt;

    public Broj() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone_category() {
        return phone_category;
    }

    public void setPhone_category(String phone_category) {
        this.phone_category = phone_category;
    }

    public Kontakt getKontakt() {
        return kontakt;
    }

    public void setKontakt(Kontakt kontakt) {
        this.kontakt = kontakt;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Override
    public String toString() {
        return phone_number + " " + phone_category;
    }
}
