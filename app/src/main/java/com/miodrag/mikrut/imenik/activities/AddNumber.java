package com.miodrag.mikrut.imenik.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.db.DatabaseHelper;
import com.miodrag.mikrut.imenik.db.model.Broj;
import com.miodrag.mikrut.imenik.db.model.Kontakt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AddNumber extends AppCompatActivity {

    private EditText number;
    private Spinner spinner;
    private Button btn_add;
    private int kontakt_id;
    private String phone_category;
    private Kontakt kontakt;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_number);

        inicijalizacija();
        setSpinner();

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNumberToTheList();
            }
        });
    }

    private void inicijalizacija(){
        number = findViewById(R.id.add_number_et_number);
        btn_add = findViewById(R.id.add_number_btn_add);

    }

    private List<String> fillSpinner(){
        List<String> list = new ArrayList<>();
        list.add("Home");
        list.add("Work");
        return list;
    }

    private void setSpinner() {
        spinner = findViewById(R.id.add_number_spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_dropdown_item,fillSpinner());
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phone_category = parent.getItemAtPosition(position).toString();
                Toast.makeText(AddNumber.this, "You choose "+phone_category, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(AddNumber.this, "Odabrana je podrazumevana vrednost "+parent.getItemAtPosition(0), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //take kontakt_id i na osnovu toga ubacuje se broj za taj kontakt.
    //posle ubacivanja vraca se na DetailActivity
    private void addNumberToTheList() {
        kontakt_id =  getIntent().getIntExtra("objekat_id", 1);

        try {
            kontakt = getDatabaseHelper().getKontaktDao().queryForId(kontakt_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Broj broj = new Broj();
        broj.setPhone_number(number.getText().toString());
        broj.setPhone_category(phone_category);
        broj.setKontakt(kontakt);

        try {
            getDatabaseHelper().getBrojDao().createIfNotExists(broj);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, Detail.class);
        intent.putExtra("objekat_id", kontakt_id);
        startActivity(intent);

    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
