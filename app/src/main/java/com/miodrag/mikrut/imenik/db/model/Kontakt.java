package com.miodrag.mikrut.imenik.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "kontakti")
public class Kontakt {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "first_name")
    private String firstName;
    @DatabaseField(columnName = "last_name")
    private String lastName;
    @DatabaseField(columnName = "image_path")
    private String image_path;
//    @DatabaseField(columnName = "phone")
//    private String phone;
    @ForeignCollectionField(foreignFieldName = "kontakt",eager = true)
    private ForeignCollection<Broj> brojevi;

    public Kontakt() {
    }

    public Kontakt(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public ForeignCollection<Broj> getBrojevi() {
        return brojevi;
    }

    public void setBrojevi(ForeignCollection<Broj> brojevi) {
        this.brojevi = brojevi;
    }

    @Override
    public String toString() {
        return firstName + " "+lastName;
    }
}
