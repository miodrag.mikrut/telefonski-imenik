package com.miodrag.mikrut.imenik.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.imenik.R;
import com.miodrag.mikrut.imenik.db.DatabaseHelper;
import com.miodrag.mikrut.imenik.db.model.Broj;
import com.miodrag.mikrut.imenik.db.model.Kontakt;
import com.miodrag.mikrut.imenik.tools.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Add extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    DatabaseHelper databaseHelper;

    private Spinner spinner;
    private EditText firstName;
    private EditText lastName;
    private EditText phoneNumber;
    private ImageButton imageButton;
    private String phone_category;
    private Button btn_add;

    private Kontakt kontakt;
    private Broj broj;
    private String image_path;
    private int objekat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        setSpinner();
        inicijalizacija();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add();
                if(Tools.proveraPrefsPodesavanja("toast",Add.this)){
                    Toast.makeText(Add.this, kontakt+ " added" , Toast.LENGTH_SHORT).show();
                }
                if(Tools.proveraPrefsPodesavanja("notifications",Add.this)){
                    Tools.createNotification(Add.this,kontakt,"add");
                }
            }
        });



    }

    private void inicijalizacija(){
        firstName = findViewById(R.id.add_et_firstName);
        lastName = findViewById(R.id.add_et_last_name);
        imageButton = findViewById(R.id.add_imagebutton);
        phoneNumber = findViewById(R.id.add_et_phone);
        btn_add = findViewById(R.id.add_btn_add);


    }

    private List<String> fillSpinner(){
        List<String> list = new ArrayList<>();
        list.add("Home");
        list.add("Work");
        return list;
    }

    private void setSpinner() {
        spinner = findViewById(R.id.spinner_new_contact);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_dropdown_item,fillSpinner());
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phone_category = parent.getItemAtPosition(position).toString();
                Toast.makeText(Add.this, "You choose "+phone_category, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(Add.this, "Odabrana je podrazumevana vrednost "+parent.getItemAtPosition(0), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void add(){
        if(Tools.validateInput(firstName)
                && Tools.validateInput(lastName)
                && Tools.validateInput(phoneNumber)

        ) {
            phone_category = spinner.getSelectedItem().toString();
            Toast.makeText(this, "Selektovano je "+ phone_category, Toast.LENGTH_SHORT).show();
            kontakt = new Kontakt();
            kontakt.setFirstName(firstName.getText().toString().trim());
            kontakt.setLastName(lastName.getText().toString().trim());
            kontakt.setImage_path(image_path);

            broj = new Broj();
            broj.setKontakt(kontakt);
            broj.setPhone_category(phone_category);
            broj.setPhone_number(phoneNumber.getText().toString().trim());

            try {
                getDatabaseHelper().getKontaktDao().create(kontakt);
                getDatabaseHelper().getBrojDao().create(broj);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            if(Tools.proveraPrefsPodesavanja("toast",Add.this)) {
                Toast.makeText(Add.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
            }
            if(Tools.proveraPrefsPodesavanja("notifications",Add.this)) {
                Tools.createNotification(this,kontakt,"add");
            }

            Intent intent = new Intent(Add.this,Main.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Da bi dobili pristup Galeriji slika na uredjaju
     * moramo preko URI-ja pristupiti delu baze gde su smestene
     * slike uredjaja. Njima mozemo pristupiti koristeci sistemski
     * ContentProvider i koristeci URI images/* putanju
     * <p>
     * Posto biramo sliku potrebno je da pozovemo aktivnost koja icekuje rezultat
     * Kada dobijemo rezultat nazad prikazemo sliku i dobijemo njenu tacnu putanju
     */
    //proverava dozvolu za biranje iz galerije
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    //ako je dozvoljeno bira se slika iz galerije
    private void selectPicture() {
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    image_path = picturePath; //uzima imageButton path i stavlja ga da bude dostupno
                    cursor.close();

                    imageButton.setImageBitmap(BitmapFactory.decodeFile(image_path));
                    // String picturePath contains the path of selected Image
                }
            }
        }
    }
//*******kraj za sliku

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
